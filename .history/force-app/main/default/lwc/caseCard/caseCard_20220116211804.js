import { LightningElement, wire, track, api } from 'lwc';
import getServicePersons from '@salesforce/apex/FieldServiceController.getServicePersons';
import updateCaseRecord from '@salesforce/apex/FieldServiceController.updateCase';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

export default class CaseCard extends NavigationMixin(LightningElement) {
    @api eachCase;
    @track caseDetail;
    selectedRow;
    caseCardName;

    //Boolean tracked variable to indicate if modal is open or not default value is false as modal is closed when page is loaded 
    @track isModalOpen = false;
    openModal() {
        this.isModalOpen = true;
    }
    closeModal() {
        this.isModalOpen = false;
    }
    submitDetails() {
        this.updateCase();
        this.isModalOpen = false;
    }

    connectedCallback() {
        this.caseCardName = this.caseDetail = this.eachCase.Account.Name + ' | ' + this.eachCase.CaseNumber + ' | ' + this.eachCase.Priority;
    }

    get caseDetails() {
        let restaurant = '';
        /*if(this.eachCase.Account.Name !== undefined && this.eachCase.Account.Name !== null && this.eachCase.Account.Name !== ''){
            restaurant = this.eachCase.Account.Name;
        }*/
        return this.caseDetail = restaurant + ' | ' + this.eachCase.CaseNumber + ' | ' + this.eachCase.Priority;
    }

    updateCase() {
        var userId = this.selectedRow[0].Id;
        var caseRecord = this.eachCase;
        updateCaseRecord({caseRecord: caseRecord, userId: userId})
            .then((result) => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: result,
                        message: 'Case assigned!',
                        variant: 'success'
                    })
                ); 
                // Display fresh data in the form
                return refreshApex(this.eachCase);
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error assigning the case',
                        message: error.body.message,
                        variant: 'error'
                    })
                );
            });
    }

    //For Assign modal
    @track columns = [{
        label: 'Service Person',
        fieldName: 'Name',
        type: 'text',
        sortable: true
    },
    {
        label: 'Mobile Phone',
        fieldName: 'Phone',
        type: 'phone',
        sortable: true
    },
    {
        label: 'Employee Number',
        fieldName: 'EmployeeNumber',
        type: 'text',
        sortable: true
    }
    ];

    @track error;
    @track spList ;
    @wire(getServicePersons)
    wiredServicePersons({
        error,
        data
    }) {
        if (data) {
            this.spList = data;
        } else if (error) {
            this.error = error;
        }
    }

    getSelectedName(event) {
        this.selectedRow = event.detail.selectedRows;
    }

    navigateToCaseDetailPage(event) {
        event.stopPropagation();
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.eachCase.Id,
                objectApiName: 'Case',
                actionName: 'view'
            },
        }).then(url => { window.open(url) });
    }

}