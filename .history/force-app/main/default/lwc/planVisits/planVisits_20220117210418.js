import { LightningElement, wire, track } from 'lwc';
import getPlannedVisits from '@salesforce/apex/FieldServiceController.getPlannedVisits';
import apexRefresh from '@salesforce/apex';

export default class PlanVisits extends LightningElement {
    
    @track eventList ;
    @wire(getPlannedVisits)
    wiredPlannedVisits({
        error,
        data
    }) {
        if(data) {
            this.eventList = data;
            apexRefresh(this.eventList); 
        } else if(error) {
            this.error = error;
        }
    } 
}