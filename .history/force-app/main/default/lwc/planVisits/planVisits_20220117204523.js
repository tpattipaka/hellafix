import { LightningElement, wire, track } from 'lwc';
import getPlannedVisits from '@salesforce/apex/FieldServiceController.getPlannedVisits';

export default class PlanVisits extends LightningElement {
    
    @track eventList ;
    @wire(getPlannedVisits)
    wiredPlannedVisits({
        error,
        data
    }) {
        if(data) {
            this.eventList = data;
            console.log('Event list:' + JSON.stringify(this.eventList));
        } else if(error) {
            this.error = error;
        }
    }
}