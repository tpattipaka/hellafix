import { LightningElement, api, track, wire } from 'lwc';
import getVisitDetails from '@salesforce/apex/FieldServiceHelper.getVisitDetails';
import { NavigationMixin } from 'lightning/navigation';

export default class ViewVisits extends NavigationMixin(LightningElement) {

    @track visit;
    @track error;
    @track visitList;
    @wire(getVisitDetails)
    wiredEventDetails({
        error,
        data
    }) {
        if (data) {
            this.visitList = data;
        } else if (error) {
            this.error = error;
        }
    }

    navigateToCaseDetailPage() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.visit.whatid,
                objectApiName: 'Case',
                actionName: 'view'
            },
        });
    }
}