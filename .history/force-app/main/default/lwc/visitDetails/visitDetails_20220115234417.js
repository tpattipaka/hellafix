import { LightningElement, track, wire, api } from 'lwc';
import getPlannedVisits from '@salesforce/apex/FieldServiceHelper.getPlannedVisits';
import updateEventRecord from '@salesforce/apex/FieldServiceHelper.updateEvent';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

export default class VisitDetails extends NavigationMixin(LightningElement) {

    @api eachEvent;
    @track eventList;
    @track eventDetail;
    @wire(getPlannedVisits)
    wiredPlannedVisits({
        error,
        data
    }) {
        if(data) {
            this.eventList = data;
        } else if(error) {
            this.error = error;
        }
    }

    get eventDetails() {
        return this.eventDetail = this.eachEvent.Subject + ' | ' + this.eachEvent.ActivityDate;
    }

    @track isModalOpen = false;
    @track isModalAccept = false;
    @track isModalReject = false;
    @track eventState;
    openAcceptModal() {
        this.isModalOpen = true;
        this.isModalAccept = true;
        this.isModalReject = false;
        this.eventState = 'Accepted';
    }
    openRejectModal() {
        this.isModalOpen = true;
        this.isModalAccept = false;
        this.isModalReject = true;
        this.eventState = 'Rejected';
    }
    closeModal() {
        this.isModalOpen = false;
    }
    submitDetails() {
        this.updateEvent();
        this.isModalOpen = false;
    }
    
    //To update Event's State to Accepted/Rejected. 
    updateEvent() {
        var eventRecord = this.eachEvent;
        var state = this.eventState;
        updateEventRecord({eventRecord: eventRecord, eventState: state})
            .then((result) => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: result,
                        message: 'Visit updated',
                        variant: 'success'
                    })
                ); 
                // Display fresh data in the form
                return refreshApex(this.eachEvent);
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error updating Visit',
                        message: error.body.message,
                        variant: 'error'
                    })
                );
            });
    }

    navigateToCaseDetailPage(event) {
        event.stopPropagation();
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.eachEvent.WhatId,
                objectApiName: 'Case',
                actionName: 'view'
            },
        }).then(url => { window.open(url) });
    }

}