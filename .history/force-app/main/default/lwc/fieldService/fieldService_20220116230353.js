import { LightningElement, wire, track } from 'lwc';
import getRepairRequests from '@salesforce/apex/FieldServiceController.getRepairRequests'; 

export default class FieldService extends LightningElement {

    @track caseList ; 
    @wire(getRepairRequests)
    wiredRepairRequest({
        error,
        data
    }) {
        if(data) { 
            this.caseList = data;
        } else if(error) {
            this.error = error;
        }
    }

    assignmentClick(event){
        this.refreshCaseList();
    }

    async refreshCaseList(){
        let result = await getRepairRequests();
        this.caseList = result;
    }
}