import { LightningElement, wire, track } from 'lwc';
import getRepairRequests from '@salesforce/apex/FieldServiceController.getRepairRequests';
import { refreshApex } from '@salesforce/apex';

export default class FieldService extends LightningElement {

    @track caseList ;
    wiredData;
    @wire(getRepairRequests)
    wiredRepairRequest({
        error,
        data
    }) {
        if(data) {
            this.wiredData = data;
            this.caseList = data;
        } else if(error) {
            this.error = error;
        }
    }

    assignmentClick(event){
        console.log('firing refresh apex')
        refreshApex(this.wiredData);
    }
}