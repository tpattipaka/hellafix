/**************************************************
Class Name                :     FieldServiceController
Test Class                :     FieldServiceControllerTest
Purpose                   :     Server side controller for field service lwc
Auther                    :     Thirupathi Pattipaka
LastModifiedBy & Date     :     Thirupathi Pattipaka on 15.01.2022   
*****************************************************/

public with sharing class FieldServiceController {

    public static Id fsQueueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperNAME = 'Field_Service_Queue' LIMIT 1].Id;
    
    @AuraEnabled(cacheable=true)
    public static List<User> getServicePersons() {
        Set<Id> unavailableIds = new Set<Id>();
        try {
            List<Event> assignedVisits = [SELECT Id, OwnerId FROM Event WHERE State__c = 'Accepted' AND ActivityDate > :System.today()];
            for(Event visit : assignedVisits) {
                //unavailableIds.add(visit.OwnerId);
            }
            return [SELECT Id, Name, EmployeeNumber, Phone FROM User WHERE IsActive = true AND Id NOT IN :unavailableIds];
        } catch (Exception e) {
            ExceptionHandler.handleGenericExceptions('FieldServiceController', 'getServicePersons', e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<Case> getRepairRequests() {
        try {
            return [SELECT Id, CaseNumber, AccountId, Account.Name, Priority, OwnerId, Status, CreatedDate, Description, Subject 
                    FROM Case WHERE OwnerId = :fsQueueId ORDER BY CreatedDate desc];
        } catch (Exception e) {
            ExceptionHandler.handleGenericExceptions('FieldServiceController', 'getRepairRequests', e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Case updateCase(Case caseRecord, Id userId) {
        List<Event> visitsToUpdate = new List<Event>();
        if(caseRecord != null) {
            caseRecord.OwnerId = userId;
            for(Event relatedEvent : [SELECT Id, OwnerId, State__c FROM Event WHERE WhatId = :caseRecord.Id]) {
                if(relatedEvent.State__c == 'Unassigned') {
                    relatedEvent.OwnerId = userId;
                    relatedEvent.State__c = 'Assigned';
                    visitsToUpdate.add(relatedEvent);
                }
            }
        }
        try {
            update caseRecord;
            update visitsToUpdate;
            return caseRecord;
        } catch (DMLException e) {
            ExceptionHandler.handleDMLExceptions('FieldServiceController', 'updateCase', e);
            throw new AuraHandledException(e.getMessage());
        } catch (Exception e) {
            ExceptionHandler.handleGenericExceptions('FieldServiceController', 'updateCase', e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<Event> getPlannedVisits() {
        Id userId = UserInfo.getUserId();
        try {
            return [SELECT Id, OwnerId, State__c, Subject, WhatId, What.Name, AccountId, ActivityDate, StartDateTime 
                    FROM Event WHERE OwnerId = :userId AND State__c = 'Assigned' ORDER BY StartDateTime desc];
        } catch (Exception e) {
            ExceptionHandler.handleGenericExceptions('FieldServiceController', 'getPlannedVisits', e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Event updateEvent(Event eventRecord, String eventState) {
        try {
            if(eventRecord != null) {
                Case relatedCase = [SELECT Id, OwnerId FROM Case WHERE Id = :eventRecord.WhatId];
                eventRecord.State__c = eventState;
                if(eventState == 'Rejected') {
                    updateCase(relatedCase, fsQueueId);
                }
            }
            update eventRecord;
            return eventRecord;
        } catch (DMLException e) {
            ExceptionHandler.handleDMLExceptions('FieldServiceController', 'updateEvent', e);
            throw new AuraHandledException(e.getMessage());
        } catch (Exception e) {
            ExceptionHandler.handleGenericExceptions('FieldServiceController', 'updateEvent', e);
            throw new AuraHandledException(e.getMessage());
        }
    }

}