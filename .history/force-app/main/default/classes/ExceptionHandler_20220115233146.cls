/********************************************************************
ClassName                : ExceptionHandler
Test Classes             : FieldServiceControllerTest
Purpose                  : To handle and save exceptions as Exception Error Log records.
**********************************************************************/

public with sharing class ExceptionHandler {
    
    public static void handleDMLExceptions(String className, String methodName, System.DMLException dmlEx) {
        ExceptionErrorLog__c errorLog = new ExceptionErrorLog__c();
        errorLog.ClassName__c = className;
        errorLog.MethodName__c = methodName;
        errorLog.Type__c = 'DML Exception';
        errorLog.LineNumber__c = dmlEx.getLineNumber();
        errorLog.ExceptionMessage__c = dmlEx.getMessage();

        insert errorLog;
    }

    public static void handleGenericExceptions(String className, String methodName, System.Exception ex) {
        ExceptionErrorLog__c errorLog = new ExceptionErrorLog__c();
        errorLog.ClassName__c = className;
        errorLog.MethodName__c = methodName;
        errorLog.Type__c = 'Generic Exception';
        errorLog.LineNumber__c = ex.getLineNumber();
        errorLog.ExceptionMessage__c = ex.getMessage();

        insert errorLog;
    }

    public static void handleDatabaseErrors(String className, String methodName, Database.Error err) {
        ExceptionErrorLog__c errorLog = new ExceptionErrorLog__c();
        errorLog.ClassName__c = className;
        errorLog.MethodName__c = methodName;
        errorLog.Type__c = 'Database Error';
        errorLog.ExceptionMessage__c = err.getMessage();

        insert errorLog;
    }

}