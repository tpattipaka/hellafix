/************************************************************
Class Name:   TestDataFactory
Purpose:      To set up test data for all test classes in the org.  
*************************************************************/

@isTest
public with sharing class TestDataFactory {
    
    //Create test Account data
    public static List<Account> createAccounts(Integer numberOfAcc) {
        List<Account> accList = new List<Account>();
        for(Integer i = 0; i < numberOfAcc; i++) {
            Account acc = new Account();
            acc.Name = 'Restaurant ' + i;
            acc.SLA__c = 'Gold';
            acc.City__c = 'Espoo';
            acc.Type = 'Customer - Direct';
            acc.Phone = '+358 403837266';
            accList.add(acc);
        }
        if(!accList.isEmpty()) {
            insert accList;
        }
        return accList;
    }

    //Create test Contact data
    public static List<Contact> createTestContacts(Integer numberOfCon, Id accountId) {
        List<Contact> conList = new List<Contact>();
        for(Integer i = 0; i < numberOfCon; i++) {
            Contact con = new Contact();
            con.LastName = 'temp_test11' + i;
            con.FirstName = 'temp_test22' + i;
            con.MobilePhone = '+358 507592388';
            con.Email = 'noreplay@' + i + 'email.com';
            con.Phone = '0505066789';
            con.AccountId = accountId;
            con.City__c = 'Espoo';
            con.Title = 'Branch Manager';
            con.PreferredLanguage__c = 'Finnish';
            conList.add(con);
        }
        if(!conList.isEmpty()) {
            insert conList;
        }
        return conList;
    }

    //Create test Case data
    public static List<Case> createTestCases(Integer numberOfCases, Id accountId, Id contactId) {
        List<Case> caseList = new List<Case>();
        for(Integer i = 0; i < numberOfCases; i++) {
            Case newCase = new Case();
            newCase.Status = 'New';
            newCase.AccountId = accountId;
            newCase.contactId = contactId;
            newCase.Priority = 'Low';
            newCase.Subject = 'Installation request at Restaurant ABC';
            caseList.add(newCase);
        }
        if(!caseList.isEmpty()) {
            insert caseList;
        }
        return caseList;
    }

    //Create test Visit data
    public static List<Event> createTestVisits(Integer numberOfVisits, Id caseId) {
        List<Event> visitList = new List<Event>();
        for(Integer i = 0; i < numberOfVisits; i++) {
            Event visit = new Event();
            visit.WhatId = caseId;
            visit.State__c = 'Unassigned';
            visit.StartDateTime = System.now().addDays(2);
            visit.EndDateTime = visit.StartDateTime.addHours(2);
            visit.Subject = 'Installation request at Restaurant ABC';
            visitList.add(visit);
        }
        if(!visitList.isEmpty()) {
            insert visitList;
        }
        return visitList;
    }

    //Create test User data
    public static User createUser(String userName, Id profileId) {
        User testUser = new User(Alias = 'tom', Email=userName, UserName = userName, 
                                ProfileId = profileId, LastName = 'Testing', LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', TimeZoneSidKey = 'Europe/Helsinki');
        insert testUser;
        return testUser;
    }
    
}