/********************************************
Class Name: FieldServiceControllerTest
Purpose: Test class for FieldServiceController
********************************************/

@isTest 
public with sharing class FieldServiceControllerTest {
    
    //Initialize test data from TestDataFactory.cls
    private static void initializeTestData() {
        List<Account> accList = TestDataFactory.createAccounts(3);
        List<Contact> conList = new List<Contact>();
        List<Case> caseList = new List<Case>();
        List<Event> eventList = new List<Event>();
        List<ExceptionErrorLog__c> logList = TestDataFactory.createErrorLogs(2);
        Id fsQueueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperNAME = 'Field_Service_Queue' LIMIT 1].Id;
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User testUser = TestDataFactory.createUser('test.user@hfx.com', profileId);

        for(Account acc : accList) {
            conList.addAll(TestDataFactory.createTestContacts(2, acc.Id));
        }
        for(Contact con : conList) {
            caseList.addAll(TestDataFactory.createTestCases(2, con.AccountId, con.Id));
        }
        for(Case c : caseList) {
            c.OwnerId = fsQueueId;
        }
        update caseList;
    }

    @isTest
    static void testRepairRequests() {
        Test.startTest();
        initializeTestData();
        List<Case> repairRequests = FieldServiceController.getRepairRequests();
        Test.stopTest();

        System.assertEquals(12, repairRequests.size());
    }

    @isTest
    static void testUpdateCase() {  
        Test.startTest();
        initializeTestData();
        Id fsQueueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperNAME = 'Field_Service_Queue' LIMIT 1].Id;
        Case caseRecord = [SELECT Id, OwnerId, Subject FROM Case LIMIT 1];
        Case repairRequest = FieldServiceController.updateCase(caseRecord, UserInfo.getUserId());
        
        List<Event> visitList = TestDataFactory.createTestVisits(2, caseRecord.Id);
        List<Event> visitListTest = FieldServiceController.getPlannedVisits(); 
        Event updatedEvent = FieldServiceController.updateEvent(visitList[0], 'Rejected');
        Case relatedCase = [SELECT Id, OwnerId FROM Case WHERE Id = :updatedEvent.WhatId];
        //FieldServiceController.notifyServicePersonWithEmail(UserInfo.getUserId(), relatedCase.Id);
        Test.stopTest();

        System.assertEquals(UserInfo.getUserId(), repairRequest.OwnerId);
        System.assertEquals(2, visitList.size());
        System.assertEquals(fsQueueId, relatedCase.OwnerId); 
    }

    @isTest
    static void testExceptions() {
        Test.startTest();
        initializeTestData();
        Case caseRecord = [SELECT Id, OwnerId FROM Case LIMIT 1];
        try {
            List<User> spList = FieldServiceController.getServicePersons();
            Case repairRequest = FieldServiceController.updateCase(caseRecord, caseRecord.Id);
        } catch(Exception e) {
            ExceptionErrorLog__c errorRec = [SELECT Id, Type__c FROM ExceptionErrorLog__c LIMIT 1];
            System.assertEquals('DML Exception', errorRec.Type__c);
        }

        try {
            Case repairRequest = FieldServiceController.updateCase(caseRecord, UserInfo.getUserId());
            List<Event> visitList = FieldServiceController.getPlannedVisits();
            Event updatedEvent = FieldServiceController.updateEvent(visitList[0], 'Unknown');
        } catch(Exception e) {
            ExceptionErrorLog__c errorRec = [SELECT Id, Type__c FROM ExceptionErrorLog__c LIMIT 1];
            System.assertEquals('DML Exception', errorRec.Type__c);
        }

        try {
            Case repairRequest = FieldServiceController.updateCase(null, null); 
        } catch(Exception e) {
            ExceptionErrorLog__c errorRec = [SELECT Id, Type__c FROM ExceptionErrorLog__c LIMIT 1];
            System.assertEquals('DML Exception', errorRec.Type__c);
        }
        Test.stopTest();
    }

}