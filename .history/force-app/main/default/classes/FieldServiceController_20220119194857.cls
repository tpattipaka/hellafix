/**************************************************
Class Name                :     FieldServiceController
Test Class                :     FieldServiceControllerTest
Purpose                   :     Server side controller for field service lwc
Auther                    :     Thirupathi Pattipaka
LastModifiedBy & Date     :     Thirupathi Pattipaka on 15.01.2022   
*****************************************************/

public with sharing class FieldServiceController {

    public static Id fsQueueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperNAME = 'Field_Service_Queue' LIMIT 1].Id;
    
    @AuraEnabled(cacheable=true)
    public static List<User> getServicePersons() {
        Set<Id> unavailableIds = new Set<Id>();
        try {
            List<Event> assignedVisits = [SELECT Id, OwnerId FROM Event WHERE State__c = 'Accepted' AND ActivityDate >= :System.today()];
            for(Event visit : assignedVisits) {
                unavailableIds.add(visit.OwnerId);
            }
            return [SELECT Id, Name, EmployeeNumber, Phone FROM User WHERE IsActive = true AND Id NOT IN :unavailableIds];
        } catch (Exception e) {
            ExceptionHandler.handleGenericExceptions('FieldServiceController', 'getServicePersons', e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<Case> getRepairRequests() {
        try {
            return [SELECT Id, CaseNumber, AccountId, Account.Name, Priority, OwnerId, Status, CreatedDate, Description, Subject, RestaurantName__c 
                    FROM Case WHERE OwnerId = :fsQueueId ORDER BY CreatedDate desc];
        } catch (Exception e) {
            ExceptionHandler.handleGenericExceptions('FieldServiceController', 'getRepairRequests', e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Case updateCase(Case caseRecord, Id userId) { 
        if(caseRecord != null) {
            caseRecord.OwnerId = userId;
            Event visit = new Event();
            visit.WhatId = caseRecord.Id;
            visit.OwnerId = userId;
            visit.State__c = 'Assigned';
            visit.StartDateTime = System.now();
            visit.EndDateTime = visit.StartDateTime.addHours(8);
            visit.Subject = caseRecord.Subject;
            insert visit; 
        }
        try {
            update caseRecord; 
            return caseRecord;
        } catch (DMLException e) {
            ExceptionHandler.handleDMLExceptions('FieldServiceController', 'updateCase', e);
            throw new AuraHandledException(e.getMessage());
        } catch (Exception e) {
            ExceptionHandler.handleGenericExceptions('FieldServiceController', 'updateCase', e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<Event> getPlannedVisits() {
        Id userId = UserInfo.getUserId();  
        try {
            return [SELECT Id, OwnerId, State__c, Subject, WhatId, What.Name, AccountId, ActivityDate, StartDateTime 
            FROM Event WHERE OwnerId = :userId AND State__c = 'Assigned' ORDER BY StartDateTime desc]; 
        } catch (Exception e) {
            ExceptionHandler.handleGenericExceptions('FieldServiceController', 'getPlannedVisits', e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Event updateEvent(Event eventRecord, String eventState) {
        try {
            if(eventRecord != null) {
                Case relatedCase = [SELECT Id, OwnerId FROM Case WHERE Id = :eventRecord.WhatId];
                eventRecord.State__c = eventState;
                if(eventState == 'Rejected') {
                    relatedCase.OwnerId = fsQueueId;
                    update relatedCase; 
                }
            }
            update eventRecord;
            return eventRecord;
        } catch (DMLException e) {
            ExceptionHandler.handleDMLExceptions('FieldServiceController', 'updateEvent', e);
            throw new AuraHandledException(e.getMessage());
        } catch (Exception e) {
            ExceptionHandler.handleGenericExceptions('FieldServiceController', 'updateEvent', e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    /*
     * Parameter				Description				  	 					Type 		Required
     * caseId 					Case info to retrieve case no., subject 		Set			True  
     * UserId					User info to retrieve Email              		Set			True  
     */ 
    private static void notifyServicePersonWithEmail(Id userId, Id caseId){
        try{
            List<Case> caseList = [Select Id, CaseNumber, owner.Email, owner.Name from Case Where Id IN : caseIds];
            if(!caseList.isEmpty()){
                List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
                for(Case c : caseList){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    List<String> sendTo = new List<String>();
                    sendTo.add(c.owner.Email);
                    mail.setToAddresses(sendTo); 
                    mail.setReplyTo('noreply@salesforce.com'); 
                    mail.setSubject('Case Attachment Deletion Notification - ' + c.CaseNumber);
                    mail.setBccSender(false);
                    mail.setUseSignature(false);
                    String body = 'Hi ' + c.owner.Name + '</b>, <br /><br />';
                    body += 'The mentioned case found banned urls in the attachment section. Hence those disallowed files have been deleted. <br /><br />';
                    
                    body += '<br /><br />'+'Regards,<br />';
                    body += 'Salesforce Support.';
                    
                    mail.setHtmlBody(body);
                    mailList.add(mail);
                } 
                Messaging.sendEmail(mailList);
            }
        }Catch(exception e){
            System.debug(e.getMessage());    
        }   
    }

}