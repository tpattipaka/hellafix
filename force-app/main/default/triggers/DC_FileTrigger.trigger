trigger DC_FileTrigger on Attachment (after insert) {
    if(Trigger.isAfter && Trigger.isInsert){
        DC_FileTriggerHandler.processCaseFiles(Trigger.new);
    }
}