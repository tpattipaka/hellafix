@isTest
public class DC_FileTriggerHandler_Test {
	
    static testMethod void fileUploaderTest()
    {
        //Create Case data
        Case cs = new Case(Subject = 'Test', Origin = 'Phone', Status = 'New');
        insert cs;
        
        //Create attachment data
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        String finalURL = 'https://test1.com;https://test2.com';
        Blob bodyBlob=Blob.valueOf(finalURL);
        attach.body=bodyBlob;
        attach.parentId=cs.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parentId=:cs.id];
        System.assertEquals(1, attachments.size());
        
        DC_SFApiKeyManagement__c ts = new DC_SFApiKeyManagement__c(Name__c = 'Sf_Oauth'); 
        ts.ApiUrl__c='https://www.abcTemp.com';
        insert ts;
        
        List<DC_SFApiKeyManagement__c> tsList = [Select Name__c from DC_SFApiKeyManagement__c Where Id=:ts.Id];
        System.assertEquals(1, tsList.size()); 
        
        Map<String, String> urlMap = new Map<String, String>();
        urlMap.put(finalURL.split(';')[0], attach.id);
        Test.setMock(HttpCalloutMock.class, new DC_FilesCalloutMock());
        Test.startTest(); 
        DC_FileTriggerHandler.deleteDisallowedFiles(finalURL, urlMap);
        Test.stopTest();
    }
}