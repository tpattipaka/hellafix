/**
 *
 * @author  thirupathi pattipaka
 * @version 1.0
 * @description To check the assigned cases for planned visits for field service person
 *
 */
import { LightningElement, wire, track } from 'lwc';
import getPlannedVisits from '@salesforce/apex/FieldServiceController.getPlannedVisits'; 
export default class PlanVisits extends LightningElement {
    
    @track eventList ;
    @wire(getPlannedVisits)
    wiredPlannedVisits({
        error,
        data
    }) {
        if(data) {
            this.eventList = data; 
        } else if(error) {
            this.error = error;
        }
    } 
}